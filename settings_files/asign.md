##gerar a chave
keytool -genkey -v -keystore ./settings_files/aqualibrio.keystore -alias aqualibrio -keyalg RSA -keysize 2048 -validity 10000
aqualibrioSuccess
Thiago Honorato
TI
Café e Pixel 
Brasília
DF
BR

## visualizar dados da chave de assinatura
- fazer isso quando precisar resgatar a chave SHA-1
keytool -list -v -keystore wiimiit.keystore -alias wiimiit
- fazer isso quando precisar resgatar a chave hash base64

echo <sha-1> | xxd -r -p | openssl base64

## export hash to facebook 

* On Windows: keytool -exportcert -alias androiddebugkey -keystore %HOMEPATH%\.android\debug.keystore | openssl sha1 -binary | openssl base64

* On Mac: keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64
c

É possível usar várias SHA-1 e várias hashes no facebook, lembre-se de adicionar isso quando for gerar a versão release. 


echo 45:16:29:48:8A:88:96:4C:E8:39:68:39:80:84:26:42:53:5F:CA:A0 | xxd -r -p | openssl base64
