# Aqualibrio App Project #

Data de criação: 23 de Agosto de 2019 

### Setup do ambiente de desenvolvimento ###

Instale o NodeJs e Ionic, considerando que você já trabalhe com o Node Version Manager, caso não, visite esta página [NMV](https://github.com/creationix/nvm)
- Node  12.8.1
- Ionic 5.2.5

```
$nvm install 12.8.1
$nvm use 5.2.5
```

### Ao Clonar o Repositório ###
1. Inicie o git flow
```$git glow init```
2. Baixe as depêndencias npm
```$npm i```

### Como fazer o Build? ###

##### Android
```
$ ionic build
$ npx cap add android
$ npx cap sync android
$ npx cap open android
```

##### IOS
```
$ ionic build
$ npx cap add ios
$ npx cap sync ios
$ npx cap open ios
```

OBS: TODOS OS ARQUIVOS DE CONFIGURAÇÃO SE ENCONTRAM NA PASTA 'settings_files'

### Referênciamento básico ###
Com Ionic Framework, nós devemos chamar referencias os paths dos assets com o padrão:
* quando usado em qualquer html
```
"assets/..."
```
* quando usado em qualquer scss
```
"../assets/..."
```
---------------------------------------------
## Autores ##
* [Thiago Honorato](https://br.linkedin.com/in/thiagohonorato) (Gerente de Projeto)
* [Yuri Samagaia](https://br.linkedin.com/in/<link>) (Developer)
